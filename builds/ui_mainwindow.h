/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *MainVLayout;
    QLabel *CalculationHistoryLabel;
    QTextEdit *Output;
    QFrame *MemoryButtonsHFrame;
    QHBoxLayout *MemoryHFrame;
    QPushButton *MemoryClear;
    QPushButton *MemoryRead;
    QPushButton *MemoryPlus;
    QPushButton *MemoryMinus;
    QPushButton *MemorySave;
    QVBoxLayout *ButtonMainVLayout;
    QGridLayout *ButtonGrid;
    QPushButton *Number_8;
    QPushButton *pushButton_10;
    QPushButton *pushButton_7;
    QPushButton *RaisedByY;
    QPushButton *Squared;
    QPushButton *RaisedBy2;
    QPushButton *Number_4;
    QPushButton *Number_5;
    QPushButton *Number_1;
    QPushButton *BracketClose;
    QPushButton *Enter;
    QPushButton *Number_2;
    QPushButton *pushButton_8;
    QPushButton *pushButton_3;
    QPushButton *pushButton_6;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QPushButton *pushButton_14;
    QPushButton *ClearCurrentNumber;
    QPushButton *pushButton_9;
    QPushButton *ClearAll;
    QPushButton *Number_7;
    QPushButton *Number_0;
    QPushButton *BracketOpen;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *Divided;
    QPushButton *Number_9;
    QPushButton *Delete;
    QPushButton *Number_3;
    QPushButton *Minus;
    QPushButton *Number_6;
    QPushButton *Plus;
    QPushButton *Times;
    QPushButton *Comma;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(574, 470);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        MainVLayout = new QVBoxLayout();
        MainVLayout->setSpacing(0);
        MainVLayout->setObjectName(QStringLiteral("MainVLayout"));
        CalculationHistoryLabel = new QLabel(centralWidget);
        CalculationHistoryLabel->setObjectName(QStringLiteral("CalculationHistoryLabel"));
        CalculationHistoryLabel->setLayoutDirection(Qt::LeftToRight);
        CalculationHistoryLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        MainVLayout->addWidget(CalculationHistoryLabel);

        Output = new QTextEdit(centralWidget);
        Output->setObjectName(QStringLiteral("Output"));
        Output->setLayoutDirection(Qt::RightToLeft);
        Output->setFrameShape(QFrame::NoFrame);
        Output->setFrameShadow(QFrame::Plain);
        Output->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        Output->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        Output->setReadOnly(true);

        MainVLayout->addWidget(Output);

        MemoryButtonsHFrame = new QFrame(centralWidget);
        MemoryButtonsHFrame->setObjectName(QStringLiteral("MemoryButtonsHFrame"));
        MemoryHFrame = new QHBoxLayout(MemoryButtonsHFrame);
        MemoryHFrame->setSpacing(0);
        MemoryHFrame->setContentsMargins(11, 11, 11, 11);
        MemoryHFrame->setObjectName(QStringLiteral("MemoryHFrame"));
        MemoryHFrame->setContentsMargins(0, 0, 0, 0);
        MemoryClear = new QPushButton(MemoryButtonsHFrame);
        MemoryClear->setObjectName(QStringLiteral("MemoryClear"));

        MemoryHFrame->addWidget(MemoryClear);

        MemoryRead = new QPushButton(MemoryButtonsHFrame);
        MemoryRead->setObjectName(QStringLiteral("MemoryRead"));

        MemoryHFrame->addWidget(MemoryRead);

        MemoryPlus = new QPushButton(MemoryButtonsHFrame);
        MemoryPlus->setObjectName(QStringLiteral("MemoryPlus"));

        MemoryHFrame->addWidget(MemoryPlus);

        MemoryMinus = new QPushButton(MemoryButtonsHFrame);
        MemoryMinus->setObjectName(QStringLiteral("MemoryMinus"));

        MemoryHFrame->addWidget(MemoryMinus);

        MemorySave = new QPushButton(MemoryButtonsHFrame);
        MemorySave->setObjectName(QStringLiteral("MemorySave"));

        MemoryHFrame->addWidget(MemorySave);


        MainVLayout->addWidget(MemoryButtonsHFrame);

        ButtonMainVLayout = new QVBoxLayout();
        ButtonMainVLayout->setSpacing(0);
        ButtonMainVLayout->setObjectName(QStringLiteral("ButtonMainVLayout"));
        ButtonGrid = new QGridLayout();
        ButtonGrid->setSpacing(0);
        ButtonGrid->setObjectName(QStringLiteral("ButtonGrid"));
        ButtonGrid->setContentsMargins(-1, -1, -1, 0);
        Number_8 = new QPushButton(centralWidget);
        Number_8->setObjectName(QStringLiteral("Number_8"));

        ButtonGrid->addWidget(Number_8, 3, 2, 1, 1);

        pushButton_10 = new QPushButton(centralWidget);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));

        ButtonGrid->addWidget(pushButton_10, 1, 4, 1, 1);

        pushButton_7 = new QPushButton(centralWidget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));

        ButtonGrid->addWidget(pushButton_7, 1, 1, 1, 1);

        RaisedByY = new QPushButton(centralWidget);
        RaisedByY->setObjectName(QStringLiteral("RaisedByY"));

        ButtonGrid->addWidget(RaisedByY, 2, 0, 1, 1);

        Squared = new QPushButton(centralWidget);
        Squared->setObjectName(QStringLiteral("Squared"));

        ButtonGrid->addWidget(Squared, 4, 0, 1, 1);

        RaisedBy2 = new QPushButton(centralWidget);
        RaisedBy2->setObjectName(QStringLiteral("RaisedBy2"));

        ButtonGrid->addWidget(RaisedBy2, 3, 0, 1, 1);

        Number_4 = new QPushButton(centralWidget);
        Number_4->setObjectName(QStringLiteral("Number_4"));

        ButtonGrid->addWidget(Number_4, 4, 1, 1, 1);

        Number_5 = new QPushButton(centralWidget);
        Number_5->setObjectName(QStringLiteral("Number_5"));

        ButtonGrid->addWidget(Number_5, 4, 2, 1, 1);

        Number_1 = new QPushButton(centralWidget);
        Number_1->setObjectName(QStringLiteral("Number_1"));

        ButtonGrid->addWidget(Number_1, 5, 1, 1, 1);

        BracketClose = new QPushButton(centralWidget);
        BracketClose->setObjectName(QStringLiteral("BracketClose"));

        ButtonGrid->addWidget(BracketClose, 6, 1, 1, 1);

        Enter = new QPushButton(centralWidget);
        Enter->setObjectName(QStringLiteral("Enter"));

        ButtonGrid->addWidget(Enter, 6, 4, 1, 1);

        Number_2 = new QPushButton(centralWidget);
        Number_2->setObjectName(QStringLiteral("Number_2"));

        ButtonGrid->addWidget(Number_2, 5, 2, 1, 1);

        pushButton_8 = new QPushButton(centralWidget);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));

        ButtonGrid->addWidget(pushButton_8, 1, 2, 1, 1);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        ButtonGrid->addWidget(pushButton_3, 0, 2, 1, 1);

        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        ButtonGrid->addWidget(pushButton_6, 1, 0, 1, 1);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        ButtonGrid->addWidget(pushButton_2, 0, 1, 1, 1);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        ButtonGrid->addWidget(pushButton, 0, 0, 1, 1);

        pushButton_14 = new QPushButton(centralWidget);
        pushButton_14->setObjectName(QStringLiteral("pushButton_14"));

        ButtonGrid->addWidget(pushButton_14, 5, 0, 1, 1);

        ClearCurrentNumber = new QPushButton(centralWidget);
        ClearCurrentNumber->setObjectName(QStringLiteral("ClearCurrentNumber"));

        ButtonGrid->addWidget(ClearCurrentNumber, 2, 1, 1, 1);

        pushButton_9 = new QPushButton(centralWidget);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));

        ButtonGrid->addWidget(pushButton_9, 1, 3, 1, 1);

        ClearAll = new QPushButton(centralWidget);
        ClearAll->setObjectName(QStringLiteral("ClearAll"));

        ButtonGrid->addWidget(ClearAll, 2, 2, 1, 1);

        Number_7 = new QPushButton(centralWidget);
        Number_7->setObjectName(QStringLiteral("Number_7"));

        ButtonGrid->addWidget(Number_7, 3, 1, 1, 1);

        Number_0 = new QPushButton(centralWidget);
        Number_0->setObjectName(QStringLiteral("Number_0"));

        ButtonGrid->addWidget(Number_0, 6, 2, 1, 1);

        BracketOpen = new QPushButton(centralWidget);
        BracketOpen->setObjectName(QStringLiteral("BracketOpen"));

        ButtonGrid->addWidget(BracketOpen, 6, 0, 1, 1);

        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        ButtonGrid->addWidget(pushButton_4, 0, 3, 1, 1);

        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        ButtonGrid->addWidget(pushButton_5, 0, 4, 1, 1);

        Divided = new QPushButton(centralWidget);
        Divided->setObjectName(QStringLiteral("Divided"));

        ButtonGrid->addWidget(Divided, 2, 4, 1, 1);

        Number_9 = new QPushButton(centralWidget);
        Number_9->setObjectName(QStringLiteral("Number_9"));

        ButtonGrid->addWidget(Number_9, 3, 3, 1, 1);

        Delete = new QPushButton(centralWidget);
        Delete->setObjectName(QStringLiteral("Delete"));

        ButtonGrid->addWidget(Delete, 2, 3, 1, 1);

        Number_3 = new QPushButton(centralWidget);
        Number_3->setObjectName(QStringLiteral("Number_3"));

        ButtonGrid->addWidget(Number_3, 5, 3, 1, 1);

        Minus = new QPushButton(centralWidget);
        Minus->setObjectName(QStringLiteral("Minus"));

        ButtonGrid->addWidget(Minus, 4, 4, 1, 1);

        Number_6 = new QPushButton(centralWidget);
        Number_6->setObjectName(QStringLiteral("Number_6"));

        ButtonGrid->addWidget(Number_6, 4, 3, 1, 1);

        Plus = new QPushButton(centralWidget);
        Plus->setObjectName(QStringLiteral("Plus"));

        ButtonGrid->addWidget(Plus, 5, 4, 1, 1);

        Times = new QPushButton(centralWidget);
        Times->setObjectName(QStringLiteral("Times"));

        ButtonGrid->addWidget(Times, 3, 4, 1, 1);

        Comma = new QPushButton(centralWidget);
        Comma->setObjectName(QStringLiteral("Comma"));

        ButtonGrid->addWidget(Comma, 6, 3, 1, 1);

        ButtonGrid->setRowStretch(0, 1);
        ButtonGrid->setRowStretch(1, 1);
        ButtonGrid->setRowStretch(2, 1);
        ButtonGrid->setRowStretch(3, 1);
        ButtonGrid->setRowStretch(4, 1);
        ButtonGrid->setRowStretch(5, 1);
        ButtonGrid->setRowStretch(6, 1);
        ButtonGrid->setColumnStretch(0, 1);
        ButtonGrid->setColumnStretch(1, 1);
        ButtonGrid->setColumnStretch(2, 1);
        ButtonGrid->setColumnStretch(3, 1);
        ButtonGrid->setColumnStretch(4, 1);
        ButtonGrid->setColumnMinimumWidth(0, 1);
        ButtonGrid->setColumnMinimumWidth(1, 1);
        ButtonGrid->setColumnMinimumWidth(2, 1);
        ButtonGrid->setColumnMinimumWidth(3, 1);
        ButtonGrid->setColumnMinimumWidth(4, 1);
        ButtonGrid->setRowMinimumHeight(0, 1);
        ButtonGrid->setRowMinimumHeight(1, 1);
        ButtonGrid->setRowMinimumHeight(2, 1);
        ButtonGrid->setRowMinimumHeight(3, 1);
        ButtonGrid->setRowMinimumHeight(4, 1);
        ButtonGrid->setRowMinimumHeight(5, 1);
        ButtonGrid->setRowMinimumHeight(6, 1);

        ButtonMainVLayout->addLayout(ButtonGrid);


        MainVLayout->addLayout(ButtonMainVLayout);

        MainVLayout->setStretch(3, 3);

        gridLayout->addLayout(MainVLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        centralWidget->setAccessibleName(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        CalculationHistoryLabel->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        Output->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"right\" dir='rtl' style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        MemoryClear->setAccessibleName(QApplication::translate("MainWindow", "MemoryButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        MemoryClear->setText(QApplication::translate("MainWindow", "MC", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        MemoryRead->setAccessibleName(QApplication::translate("MainWindow", "MemoryButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        MemoryRead->setText(QApplication::translate("MainWindow", "MR", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        MemoryPlus->setAccessibleName(QApplication::translate("MainWindow", "MemoryButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        MemoryPlus->setText(QApplication::translate("MainWindow", "M+", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        MemoryMinus->setAccessibleName(QApplication::translate("MainWindow", "MemoryButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        MemoryMinus->setText(QApplication::translate("MainWindow", "M-", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        MemorySave->setAccessibleName(QApplication::translate("MainWindow", "MemoryButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        MemorySave->setText(QApplication::translate("MainWindow", "MS", Q_NULLPTR));
        Number_8->setText(QApplication::translate("MainWindow", "8", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton_10->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton_10->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton_7->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton_7->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
        RaisedByY->setText(QApplication::translate("MainWindow", "x^y", Q_NULLPTR));
        Squared->setText(QApplication::translate("MainWindow", "\342\210\232", Q_NULLPTR));
        RaisedBy2->setText(QApplication::translate("MainWindow", "x^2", Q_NULLPTR));
        Number_4->setText(QApplication::translate("MainWindow", "4", Q_NULLPTR));
        Number_5->setText(QApplication::translate("MainWindow", "5", Q_NULLPTR));
        Number_1->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        BracketClose->setAccessibleName(QApplication::translate("MainWindow", "ButtonBottomRow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        BracketClose->setText(QApplication::translate("MainWindow", ")", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        Enter->setAccessibleName(QApplication::translate("MainWindow", "ButtonBottomRow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        Enter->setText(QApplication::translate("MainWindow", "=", Q_NULLPTR));
        Number_2->setText(QApplication::translate("MainWindow", "2", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton_8->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton_8->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton_3->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton_3->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton_6->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton_6->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton_2->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton_2->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
        pushButton_14->setText(QApplication::translate("MainWindow", "(-)", Q_NULLPTR));
        ClearCurrentNumber->setText(QApplication::translate("MainWindow", "CE", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton_9->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton_9->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
        ClearAll->setText(QApplication::translate("MainWindow", "C", Q_NULLPTR));
        Number_7->setText(QApplication::translate("MainWindow", "7", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        Number_0->setAccessibleName(QApplication::translate("MainWindow", "ButtonBottomRow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        Number_0->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        BracketOpen->setAccessibleName(QApplication::translate("MainWindow", "ButtonBottomRow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        BracketOpen->setText(QApplication::translate("MainWindow", "(", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton_4->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton_4->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        pushButton_5->setAccessibleName(QApplication::translate("MainWindow", "AdvancedButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        pushButton_5->setText(QApplication::translate("MainWindow", "XXX", Q_NULLPTR));
        Divided->setText(QApplication::translate("MainWindow", "/", Q_NULLPTR));
        Number_9->setText(QApplication::translate("MainWindow", "9", Q_NULLPTR));
        Delete->setText(QApplication::translate("MainWindow", "Del", Q_NULLPTR));
        Number_3->setText(QApplication::translate("MainWindow", "3", Q_NULLPTR));
        Minus->setText(QApplication::translate("MainWindow", "-", Q_NULLPTR));
        Number_6->setText(QApplication::translate("MainWindow", "6", Q_NULLPTR));
        Plus->setText(QApplication::translate("MainWindow", "+", Q_NULLPTR));
        Times->setText(QApplication::translate("MainWindow", "X", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        Comma->setAccessibleName(QApplication::translate("MainWindow", "ButtonBottomRow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        Comma->setText(QApplication::translate("MainWindow", ",", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
