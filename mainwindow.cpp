#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Ladda stylesheet
    QFile style(":/style.css");
    style.open(QFile::ReadOnly);
    QString stylesheet = QLatin1String(style.readAll());
    ui->centralWidget->setStyleSheet(stylesheet);

    m_connectNumberButtons();
}

// Connector functions
void MainWindow::m_connectNumberButtons()
{
    connect(ui->Number_0, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
    connect(ui->Number_1, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
    connect(ui->Number_2, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
    connect(ui->Number_3, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
    connect(ui->Number_4, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
    connect(ui->Number_5, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
    connect(ui->Number_6, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
    connect(ui->Number_7, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
    connect(ui->Number_8, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
    connect(ui->Number_9, SIGNAL(clicked()),
            this, SLOT(slot_numberButtonClicked()));
}

// Helper functions
QString MainWindow::m_getCurrentOutput()
{
    return ui->Output->toPlainText();
}

void MainWindow::m_writeToOutput(QString out)
{
    ui->Output->setText(out);
}

// Slots
void MainWindow::slot_numberButtonClicked()
{
    QString btnName = QWidget::sender()->objectName();
    QString number = QString(btnName[btnName.length()-1]);

    QString output = m_getCurrentOutput();

    if (output[0] == "0")
    {
        m_writeToOutput(number);
    }
    else
    {
        m_writeToOutput(output + number);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
