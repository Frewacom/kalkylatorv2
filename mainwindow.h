#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void slot_numberButtonClicked();

private:
    Ui::MainWindow *ui;

    void m_connectNumberButtons();
    void m_writeToOutput(QString out);

    QString m_getCurrentOutput();
};

#endif // MAINWINDOW_H
